Guide to run the api:

- run 'composer install'
- create 'lig_api' database on mysql
- run 'php artisan migrate'
- run 'php artisan serve'

Features used:
- repository pattern
- jwt authentication
- laravel policy for authorization

