<?php

namespace App\Http\Controllers\Posts;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CommentResource;
use App\Http\Resources\AllCommentsResource;
use App\Repositories\Contracts\IPost;
use App\Repositories\Contracts\IComment;

use App\Repositories\Eloquent\Criteria\{
    EagerLoad
};


class CommentController extends Controller
{

    protected $comments;
    protected $posts;

    public function __construct(IComment $comments, IPost $posts)
    {
        $this->comments = $comments;
        $this->posts = $posts;
    }


    public function index($slug)
    {
        $post = $this->posts->findWhere('slug', $slug);
            if(!$post->isEmpty()){
                $comments = $this->posts->withCriteria([
                    new EagerLoad(['comments'])
                ])->findWhereFirst('slug', $slug);
                return new AllCommentsResource($comments);
            }else{
                return response()->json(['message' => 'No query results for model [App\\Models\\Post]'], 404);
            }
    }

    public function store(Request $request, $slug)
    {
        $this->validate($request, [
            'body' => ['required']
        ]);

        $comment = $this->posts->addCommentBySlug($slug, [
            'body' => $request->body,
            'creator_id' => auth()->id()
        ]);
       if($comment !== null){
            return new CommentResource($comment);
       }
       return response()->json(['message' => 'No query results for model [App\\Models\\Post]'], 404);
    }

    public function update(Request $request, $slug, $id)
    {
        $this->validate($request, [
            'body' => ['required']
        ]);

        $post = $this->posts->withCriteria([
            new EagerLoad(['user'])
        ])->findWhereFirst('slug', $slug);

       if($post !== null){

            $comment = $this->comments->find($id);
            $response = \Gate::inspect('update', $comment);
            if ($response->allowed()) {
                $this->validate($request, [
                    'body' => ['required']
                ]);
                $comment = $this->comments->update($id, [
                    'body' => $request->body
                ]);
                return new CommentResource($comment);
            } else {
                return response()->json(['message' => $response->message() ], 403);
            }

       }else{
        return response()->json(['message' => 'No query results for model [App\\Models\\Post]'], 404);
       }


    }



    public function delete(Request $request, $slug, $id)
    {

        $post = $this->posts->withCriteria([
            new EagerLoad(['user'])
        ])->findWhereFirst('slug', $slug);

       if($post !== null){

            $comment = $this->comments->find($id);
            $response = \Gate::inspect('delete', $comment);
            if ($response->allowed()) {
            $this->comments->delete($id);
            return response()->json(['message' => 'record deleted successfully'], 200);
            } else {
                return response()->json(['message' => $response->message() ], 403);
            }

       }else{
        return response()->json(['message' => 'No query results for model [App\\Models\\Post]'], 404);
       }


    }


}
