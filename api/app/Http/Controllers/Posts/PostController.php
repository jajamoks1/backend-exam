<?php

namespace App\Http\Controllers\Posts;

use App\Models\Post;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;
use App\Repositories\Contracts\IPost;
use Illuminate\Support\Facades\Storage;
use App\Repositories\Eloquent\Criteria\{
    LatestFirst,
    ForUser,
    EagerLoad
};

class PostController extends Controller
{
    protected $posts;

    public function __construct(IPost $posts)
    {
        $this->posts = $posts;
    }

    public function index()
    {
        $posts = $this->posts->withCriteria([
            new LatestFirst(),
            new EagerLoad(['user'])
        ])->paginate(20);
        if(!$posts->isEmpty()){
            return PostResource::collection($posts);
        }
        return response()->json(['message' => 'No query results for model [App\\Models\\Post]'], 404);

    }




    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => ['required', 'string', 'max:80', 'unique:posts,title']
        ]);

        $post = $this->posts->create([
            'title' => $request->title,
            'content' => $request->content,
            'slug' => Str::slug($request->title),
            'user_id' => auth()->id(),
        ]);

        return new PostResource($post);

        }

    public function update(Request $request, $slug)
    {
        $post = $this->posts->findWhereFirst('slug', $slug);
        $this->authorize('update', $post);
        $this->validate($request, [
            'title' => ['required', 'unique:posts,title,'. $post->id],
            'content' => ['required', 'string', 'min:20', 'max:140']
        ]);

        $post = $this->posts->update($post->id, [
            'title' => $request->title,
            'content' => $request->content,
            'slug' => Str::slug($request->title)
        ]);

        return new PostResource($post);
    }



    public function delete($slug)
    {
        $post = $this->posts->findWhereFirst('slug', $slug);
        $this->authorize('delete', $post);
        $this->posts->delete($post->id);
        return response()->json(['message' => 'record deleted successfully'], 200);

    }





}
