<?php

namespace App\Policies;

use App\Models\Comment;
use Illuminate\Auth\Access\Response;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy
{
    use HandlesAuthorization;


    public function viewAny(User $user)
    {
        //
    }


    public function view(User $user, Comment $comment)
    {
        //
    }

    /**
     * Determine whether the user can create comments.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }


    public function update(User $user, Comment $comment)
    {
        return $comment->creator_id == $user->id
                ? Response::allow()
                : Response::deny('You cannot update the comment of other users');
    }


    public function delete(User $user, Comment $comment)
    {
        return $comment->creator_id == $user->id
                ? Response::allow()
                : Response::deny('You cannot delete the comment of other users');
    }

    public function restore(User $user, Comment $comment)
    {
        //
    }

    public function forceDelete(User $user, Comment $comment)
    {
        //
    }
}
