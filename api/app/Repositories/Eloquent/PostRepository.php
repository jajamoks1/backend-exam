<?php
namespace App\Repositories\Eloquent;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Repositories\Contracts\IPost;
use App\Repositories\Eloquent\BaseRepository;

class PostRepository extends BaseRepository implements IPost
{

    public function model()
    {
        return Post::class;
    }


    public function addComment($postId, array $data)
    {
        // get the post for which we want to create a comment
        $post = $this->find($postId);

        // create the comment for the post
        $comment = $post->comments()->create($data);
        return $comment;
    }



    public function addCommentBySlug($slug, array $data)
    {
        // get the post for which we want to create a comment
        $post = $this->model
                ->where('slug', $slug)
                ->first();
        // create the comment for the post
        if($post){
            $comment = $post->comments()->create($data);
            return $comment;
        }else{
            return null;
        }


    }

}
