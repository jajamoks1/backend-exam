<?php

namespace App\Repositories\Contracts;

use Illuminate\Http\Request;

interface IPost
{
    public function addComment($postId, array $data);
    public function addCommentBySlug($slug, array $data);

}
