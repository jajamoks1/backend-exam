<?php


// Route group for authenticated users only
Route::group(['middleware' => ['auth:api']], function(){
    Route::post('logout', 'Auth\LoginController@logout');

    // Posts
    Route::post('post', 'Posts\PostController@store');
    Route::put('posts/{post}', 'Posts\PostController@update');
    Route::delete('posts/{post}', 'Posts\PostController@delete');

    // Comments
    Route::post('posts/{post}/comments', 'Posts\CommentController@store');
    Route::put('posts/{post}/comments/{id}', 'Posts\CommentController@update');
    Route::delete('posts/{post}/comments/{id}', 'Posts\CommentController@delete');

});

// Routes for guests only
Route::group(['middleware' => ['guest:api']], function(){
    Route::post('register', 'Auth\RegisterController@register');
    Route::post('login', 'Auth\LoginController@login');


    // posts
    Route::get('posts', 'Posts\PostController@index');
    Route::get('posts/{slug}', 'Posts\PostController@findBySlug');
    Route::get('posts/{post}/comments', 'Posts\CommentController@index');

});

